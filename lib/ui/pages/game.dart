import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:puissance4/cubit/activity/activity_cubit.dart';
import 'package:puissance4/models/activity/activity.dart';
import 'package:puissance4/ui/widgets/game/game_board.dart';
import 'package:puissance4/ui/widgets/game/game_bottom.dart';
import 'package:puissance4/ui/widgets/game/game_end.dart';

class PageGame extends StatelessWidget {
  const PageGame({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        return Container(
          alignment: AlignmentDirectional.topCenter,
          padding: const EdgeInsets.all(4),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(height: 48),
              const GameBoardWidget(),
              const SizedBox(height: 16),
              const GameBottomWidget(),
              const Expanded(child: SizedBox.shrink()),
              currentActivity.isFinished ? const GameEndWidget() : const SizedBox.shrink(),
            ],
          ),
        );
      },
    );
  }
}
