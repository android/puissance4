import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:puissance4/config/application_config.dart';

import 'package:puissance4/cubit/activity/activity_cubit.dart';
import 'package:puissance4/models/activity/activity.dart';

class GameEndWidget extends StatelessWidget {
  const GameEndWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        const Image decorationImage = Image(
          image: AssetImage('assets/ui/placeholder.png'),
          fit: BoxFit.fill,
        );

        final double width = MediaQuery.of(context).size.width;

        final bool connected4 = currentActivity.connected4();

        final String player1ImageAsset = (currentActivity.currentPlayer == 1 && connected4)
            ? 'assets/ui/game_win.png'
            : (currentActivity.currentPlayer == 2 && connected4)
                ? 'assets/ui/game_fail.png'
                : 'assets/ui/game_draw.png';
        final String player2ImageAsset = (currentActivity.currentPlayer == 2 && connected4)
            ? 'assets/ui/game_win.png'
            : (currentActivity.currentPlayer == 1 && connected4)
                ? 'assets/ui/game_fail.png'
                : 'assets/ui/game_draw.png';

        final Image player1Image = Image(
          image: AssetImage(player1ImageAsset),
          fit: BoxFit.fill,
        );

        final Image player2Image = Image(
          image: AssetImage(player2ImageAsset),
          fit: BoxFit.fill,
        );

        final Widget indicatorPlayer1 = (currentActivity.currentPlayer == 1 && connected4)
            ? AspectRatio(
                aspectRatio: 1,
                child: StyledButton(
                  color: Colors.yellow,
                  onPressed: () {},
                  child: SizedBox.shrink(),
                ),
              )
            : player1Image;

        final Widget indicatorPlayer2 = (currentActivity.currentPlayer == 2 && connected4)
            ? AspectRatio(
                aspectRatio: 1,
                child: StyledButton(
                  color: Colors.red,
                  onPressed: () {},
                  child: SizedBox.shrink(),
                ),
              )
            : player2Image;

        return Container(
          margin: const EdgeInsets.all(2),
          padding: const EdgeInsets.all(2),
          child: Table(
            defaultColumnWidth: FixedColumnWidth(width / 5.1),
            defaultVerticalAlignment: TableCellVerticalAlignment.bottom,
            children: [
              TableRow(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [indicatorPlayer1],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [player1Image],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      currentActivity.animationInProgress == true
                          ? decorationImage
                          : ActivityButtonQuit(
                              onPressed: () {
                                BlocProvider.of<ActivityCubit>(context).quitActivity();
                                BlocProvider.of<NavCubitPage>(context)
                                    .updateIndex(ApplicationConfig.activityPageIndexHome);
                              },
                              color: Colors.blue,
                            ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [player2Image],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [indicatorPlayer2],
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
