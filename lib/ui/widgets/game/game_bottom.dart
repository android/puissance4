import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:puissance4/cubit/activity/activity_cubit.dart';
import 'package:puissance4/models/activity/activity.dart';

class GameBottomWidget extends StatelessWidget {
  const GameBottomWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        if (currentActivity.isFinished) {
          return SizedBox.shrink();
        }

        final Color color = currentActivity.currentPlayer == 0
            ? Colors.grey
            : (currentActivity.currentPlayer == 1 ? Colors.yellow : Colors.red);

        return StyledButton(
          color: color,
          onPressed: () {},
          child: SizedBox.shrink(),
        );
      },
    );
  }
}
