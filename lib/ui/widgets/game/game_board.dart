import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:puissance4/cubit/activity/activity_cubit.dart';

class GameBoardWidget extends StatelessWidget {
  const GameBoardWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: BlocBuilder<ActivityCubit, ActivityState>(
        builder: (BuildContext context, ActivityState activityState) {
          final Color borderColor = Theme.of(context).colorScheme.onSurface;

          Widget cellContent({required int row, required int col}) {
            final cellValue =
                activityState.currentActivity.board.getCellValue(row: row, col: col);
            final Color color =
                cellValue == 0 ? Colors.grey : (cellValue == 1 ? Colors.yellow : Colors.red);

            return AspectRatio(
              aspectRatio: 1,
              child: StyledButton(
                color: color,
                onPressed: () {
                  BlocProvider.of<ActivityCubit>(context).tapOnColumn(col: col);
                },
                child: SizedBox.shrink(),
              ),
            );
          }

          return Container(
            margin: const EdgeInsets.all(2),
            padding: const EdgeInsets.all(2),
            decoration: BoxDecoration(
              color: borderColor,
              borderRadius: BorderRadius.circular(8),
              border: Border.all(
                color: borderColor,
                width: 2,
              ),
            ),
            child: Table(
              defaultColumnWidth: const IntrinsicColumnWidth(),
              children: [
                for (int row = 0; row < activityState.currentActivity.sizeVertical; row++)
                  TableRow(
                    children: [
                      for (int col = 0;
                          col < activityState.currentActivity.sizeHorizontal;
                          col++)
                        Column(
                          children: [cellContent(row: row, col: col)],
                        ),
                    ],
                  ),
              ],
            ),
          );
        },
      ),
    );
  }
}
