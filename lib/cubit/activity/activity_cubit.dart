import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:puissance4/models/activity/activity.dart';
import 'package:puissance4/robot/robot_player.dart';

part 'activity_state.dart';

class ActivityCubit extends HydratedCubit<ActivityState> {
  ActivityCubit()
      : super(ActivityState(
          currentActivity: Activity.createNull(),
        ));

  void updateState(Activity activity) {
    emit(ActivityState(
      currentActivity: activity,
    ));
  }

  void refresh() {
    final Activity activity = Activity(
      // Settings
      activitySettings: state.currentActivity.activitySettings,
      // State
      isRunning: state.currentActivity.isRunning,
      isStarted: state.currentActivity.isStarted,
      isFinished: state.currentActivity.isFinished,
      animationInProgress: state.currentActivity.animationInProgress,
      // Base data
      board: state.currentActivity.board,
      sizeHorizontal: state.currentActivity.sizeHorizontal,
      sizeVertical: state.currentActivity.sizeVertical,
      // Game data
      currentPlayer: state.currentActivity.currentPlayer,
    );
    // game.dump();

    updateState(activity);
  }

  void startNewActivity(BuildContext context) {
    final ActivitySettingsCubit activitySettingsCubit =
        BlocProvider.of<ActivitySettingsCubit>(context);

    final Activity newActivity = Activity.createNew(
      // Settings
      activitySettings: activitySettingsCubit.state.settings,
    );

    newActivity.dump();

    updateState(newActivity);
    refresh();

    robotPlay();
  }

  bool canBeResumed() {
    return state.currentActivity.canBeResumed;
  }

  void quitActivity() {
    state.currentActivity.isRunning = false;
    refresh();
  }

  void resumeSavedActivity() {
    state.currentActivity.isRunning = true;
    refresh();
  }

  void deleteSavedActivity() {
    state.currentActivity.isRunning = false;
    state.currentActivity.isFinished = true;
    refresh();
  }

  void toggleCurrentPlayer() {
    state.currentActivity.currentPlayer = 3 - state.currentActivity.currentPlayer;
    refresh();

    robotPlay();
  }

  void robotPlay() async {
    if (!state.currentActivity.isFinished && !state.currentActivity.isCurrentPlayerHuman()) {
      final int? pickedCell = RobotPlayer.pickColumn(state.currentActivity);
      await Future.delayed(const Duration(milliseconds: 500));

      if (pickedCell != null) {
        tapOnColumn(col: pickedCell);
      }
    }
  }

  void tapOnColumn({required int col}) async {
    printlog('tap on column: $col');

    if (!state.currentActivity.isMoveAllowed(col: col)) {
      printlog('not allowed');
      return;
    }

    state.currentActivity.animationInProgress = true;
    refresh();

    await dropCoin(col: col);

    if (state.currentActivity.connected4()) {
      printlog('user connected 4');
      state.currentActivity.isFinished = true;
      refresh();
    }
    if (!state.currentActivity.canPlay()) {
      printlog('user has no more move to play');
      state.currentActivity.isFinished = true;
      refresh();
    }

    state.currentActivity.animationInProgress = false;
    if (state.currentActivity.isFinished == false) {
      toggleCurrentPlayer();
    }

    refresh();
  }

  // put coin on top cell
  // and make it fall until bottom or other coin
  Future<void> dropCoin({required int col}) async {
    int currentRow = 0;

    do {
      if (currentRow > 0) {
        state.currentActivity.board.setCellValue(
          row: currentRow - 1,
          col: col,
          value: 0,
        );
      }
      state.currentActivity.board.setCellValue(
        row: currentRow++,
        col: col,
        value: state.currentActivity.currentPlayer,
      );
      refresh();

      await Future.delayed(const Duration(milliseconds: 50));
    } while (currentRow < state.currentActivity.sizeVertical &&
        state.currentActivity.board.getCellValue(
              row: currentRow,
              col: col,
            ) ==
            0);

    refresh();
  }

  @override
  ActivityState? fromJson(Map<String, dynamic> json) {
    final Activity currentActivity = json['currentActivity'] as Activity;

    return ActivityState(
      currentActivity: currentActivity,
    );
  }

  @override
  Map<String, dynamic>? toJson(ActivityState state) {
    return <String, dynamic>{
      'currentActivity': state.currentActivity.toJson(),
    };
  }
}
