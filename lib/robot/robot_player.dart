import 'package:puissance4/models/activity/activity.dart';

class RobotPlayer {
  static int? pickColumn(Activity currentActivity) {
    List<int> allowedColumns = [];

    for (int columnIndex = 0; columnIndex < currentActivity.sizeHorizontal; columnIndex++) {
      if (currentActivity.isMoveAllowed(col: columnIndex)) {
        allowedColumns.add(columnIndex);
      }
    }

    if (allowedColumns.isEmpty) {
      return null;
    }

    allowedColumns.shuffle();

    final int pickedColumn = allowedColumns[0];

    return pickedColumn;
  }
}
