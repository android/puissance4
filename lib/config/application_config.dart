import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:puissance4/cubit/activity/activity_cubit.dart';

import 'package:puissance4/ui/pages/game.dart';

class ApplicationConfig {
  // activity parameter: game mode
  static const String parameterCodeGameMode = 'activity.gameMode';
  static const String gameModeHumanVsHuman = 'human-vs-human';
  static const String gameModeHumanVsRobot = 'human-vs-robot';
  static const String gameModeRobotVsHuman = 'robot-vs-human';
  static const String gameModeRobotVsRobot = 'robot-vs-robot';

  // activity parameter: board size
  static const String parameterCodeBoardSize = 'activity.size';
  static const String boardSizeValueMedium = '7x6';

  // activity pages
  static const int activityPageIndexHome = 0;
  static const int activityPageIndexGame = 1;

  static final ApplicationConfigDefinition config = ApplicationConfigDefinition(
    appTitle: 'Puissance4',
    activitySettings: [
      // game mode
      ApplicationSettingsParameter(
        code: parameterCodeGameMode,
        values: [
          ApplicationSettingsParameterItemValue(
            value: gameModeHumanVsHuman,
            color: Colors.green,
            text: '🧑⚡🧑',
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: gameModeRobotVsRobot,
            color: Colors.brown,
            text: '🤖⚡🤖',
          ),
          ApplicationSettingsParameterItemValue(
            value: gameModeRobotVsHuman,
            color: Colors.pink,
            text: '🤖⚡🧑',
          ),
          ApplicationSettingsParameterItemValue(
            value: gameModeHumanVsRobot,
            color: Colors.pink,
            text: '🧑⚡🤖',
          ),
        ],
        itemsPerLine: 2,
      ),

      // board size
      ApplicationSettingsParameter(
        code: parameterCodeBoardSize,
        values: [
          ApplicationSettingsParameterItemValue(
            value: boardSizeValueMedium,
            isDefault: true,
          ),
        ],
      ),
    ],
    startNewActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).startNewActivity(context);
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    quitCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).quitActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexHome);
    },
    deleteCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).deleteSavedActivity();
    },
    resumeActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).resumeSavedActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    navigation: ApplicationNavigation(
      screenActivity: ScreenItem(
        code: 'screen_activity',
        icon: Icon(UniconsLine.home),
        screen: ({required ApplicationConfigDefinition appConfig}) =>
            ScreenActivity(appConfig: appConfig),
      ),
      screenSettings: ScreenItem(
        code: 'screen_settings',
        icon: Icon(UniconsLine.setting),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenSettings(),
      ),
      screenAbout: ScreenItem(
        code: 'screen_about',
        icon: Icon(UniconsLine.info_circle),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenAbout(),
      ),
      activityPages: {
        activityPageIndexHome: ActivityPageItem(
          code: 'page_home',
          icon: Icon(UniconsLine.home),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageParameters(
                  appConfig: appConfig,
                  canBeResumed: activityState.currentActivity.canBeResumed,
                );
              },
            );
          },
        ),
        activityPageIndexGame: ActivityPageItem(
          code: 'page_game',
          icon: Icon(UniconsLine.star),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageGame();
              },
            );
          },
        ),
      },
    ),
  );
}
