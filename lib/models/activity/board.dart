import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

typedef BoardCells = List<List<int>>;

class Board {
  Board({
    required this.cells,
  });

  BoardCells cells = const [];

  factory Board.createNull() {
    return Board(
      cells: [],
    );
  }

  factory Board.createNew({
    required BoardCells cells,
  }) {
    return Board(
      cells: cells,
    );
  }

  int getCellValue({required int row, required int col}) {
    return cells[row][col];
  }

  void setCellValue({required int row, required int col, required int value}) {
    cells[row][col] = value;
  }

  void dump() {
    printlog('');
    String line = '--';
    for (int i = 0; i < cells[0].length; i++) {
      line += '-';
    }
    printlog(line);
    for (int rowIndex = 0; rowIndex < cells.length; rowIndex++) {
      String currentLine = '';
      for (int colIndex = 0; colIndex < cells[rowIndex].length; colIndex++) {
        currentLine += getCellValue(row: rowIndex, col: colIndex).toString();
      }
      printlog('|$currentLine|');
    }
    printlog(line);
    printlog('');
  }

  @override
  String toString() {
    return '$Board(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      'cells': cells,
    };
  }
}
