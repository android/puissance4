import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:puissance4/config/application_config.dart';
import 'package:puissance4/models/activity/board.dart';

class Activity {
  Activity({
    // Settings
    required this.activitySettings,

    // State
    this.isRunning = false,
    this.isStarted = false,
    this.isFinished = false,
    this.animationInProgress = false,

    // Base data
    required this.board,
    required this.sizeHorizontal,
    required this.sizeVertical,

    // Game data
    required this.currentPlayer,
  });

  // Settings
  final ActivitySettings activitySettings;

  // State
  bool isRunning;
  bool isStarted;
  bool isFinished;
  bool animationInProgress;

  // Base data
  Board board;
  final int sizeHorizontal;
  final int sizeVertical;

  // Game data
  int currentPlayer;

  factory Activity.createNull() {
    return Activity(
      // Settings
      activitySettings: ActivitySettings.createDefault(appConfig: ApplicationConfig.config),
      // Base data
      board: Board.createNull(),
      sizeHorizontal: 0,
      sizeVertical: 0,
      // Game data
      currentPlayer: 0,
    );
  }

  factory Activity.createNew({
    ActivitySettings? activitySettings,
  }) {
    final ActivitySettings newActivitySettings = activitySettings ??
        ActivitySettings.createDefault(appConfig: ApplicationConfig.config);

    final int sizeHorizontal = int.parse(
        newActivitySettings.get(ApplicationConfig.parameterCodeBoardSize).split('x')[0]);
    final int sizeVertical = int.parse(
        newActivitySettings.get(ApplicationConfig.parameterCodeBoardSize).split('x')[1]);

    // Create empty board (it will be mined after first hit)
    final BoardCells board = [];
    for (int rowIndex = 0; rowIndex < sizeVertical; rowIndex++) {
      final List<int> row = [];
      for (int colIndex = 0; colIndex < sizeHorizontal; colIndex++) {
        row.add(0);
      }
      board.add(row);
    }

    return Activity(
      // Settings
      activitySettings: newActivitySettings,
      // State
      isRunning: true,
      // Base data
      board: Board.createNew(cells: board),
      sizeHorizontal: sizeHorizontal,
      sizeVertical: sizeVertical,
      // Game data
      currentPlayer: 1,
    );
  }

  bool get canBeResumed => isStarted && !isFinished;

  // Ensure move is allowed
  bool isMoveAllowed({required int col}) {
    // ensure first row is empty on selected col
    return board.getCellValue(row: 0, col: col) == 0;
  }

  bool connected4() {
    // vertical
    for (int row = 0; row < sizeVertical - 3; row++) {
      for (int col = 0; col < sizeHorizontal; col++) {
        if ((board.getCellValue(row: row, col: col) == currentPlayer) &&
            (board.getCellValue(row: row + 1, col: col) == currentPlayer) &&
            (board.getCellValue(row: row + 2, col: col) == currentPlayer) &&
            (board.getCellValue(row: row + 3, col: col) == currentPlayer)) {
          return true;
        }
      }
    }

    // horizontal
    for (int row = 0; row < sizeVertical; row++) {
      for (int col = 0; col < sizeHorizontal - 3; col++) {
        if ((board.getCellValue(row: row, col: col) == currentPlayer) &&
            (board.getCellValue(row: row, col: col + 1) == currentPlayer) &&
            (board.getCellValue(row: row, col: col + 2) == currentPlayer) &&
            (board.getCellValue(row: row, col: col + 3) == currentPlayer)) {
          return true;
        }
      }
    }

    // diagonal down
    for (int row = 0; row < sizeVertical - 3; row++) {
      for (int col = 0; col < sizeHorizontal - 3; col++) {
        if ((board.getCellValue(row: row, col: col) == currentPlayer) &&
            (board.getCellValue(row: row + 1, col: col + 1) == currentPlayer) &&
            (board.getCellValue(row: row + 2, col: col + 2) == currentPlayer) &&
            (board.getCellValue(row: row + 3, col: col + 3) == currentPlayer)) {
          return true;
        }
      }
    }

    // diagonal up
    for (int row = 0; row < sizeVertical - 3; row++) {
      for (int col = 0; col < sizeHorizontal - 3; col++) {
        if ((board.getCellValue(row: row + 3, col: col) == currentPlayer) &&
            (board.getCellValue(row: row + 2, col: col + 1) == currentPlayer) &&
            (board.getCellValue(row: row + 1, col: col + 2) == currentPlayer) &&
            (board.getCellValue(row: row, col: col + 3) == currentPlayer)) {
          return true;
        }
      }
    }

    return false;
  }

  bool canPlay() {
    List<int> allowedColumns = [];

    for (int columnIndex = 0; columnIndex < sizeHorizontal; columnIndex++) {
      if (isMoveAllowed(col: columnIndex)) {
        allowedColumns.add(columnIndex);
      }
    }

    return allowedColumns.isNotEmpty;
  }

  bool isPlayerHuman(int playerIndex) {
    switch (activitySettings.get(ApplicationConfig.parameterCodeGameMode)) {
      case ApplicationConfig.gameModeHumanVsHuman:
        return true;
      case ApplicationConfig.gameModeHumanVsRobot:
        return (playerIndex == 1);
      case ApplicationConfig.gameModeRobotVsHuman:
        return (playerIndex == 2);
      case ApplicationConfig.gameModeRobotVsRobot:
        return false;
      default:
    }
    return true;
  }

  bool isCurrentPlayerHuman() {
    return isPlayerHuman(currentPlayer);
  }

  void dump() {
    printlog('');
    printlog('## Current game dump:');
    printlog('');
    printlog('$Activity:');
    printlog('  Settings');
    activitySettings.dump();
    printlog('  State');
    printlog('    isRunning: $isRunning');
    printlog('    isStarted: $isStarted');
    printlog('    isFinished: $isFinished');
    printlog('    animationInProgress: $animationInProgress');
    board.dump();
    printlog('  Game data');
    printlog('    currentPlayer: $currentPlayer');
    printlog('');
  }

  @override
  String toString() {
    return '$Activity(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      // Settings
      'activitySettings': activitySettings.toJson(),
      // State
      'isRunning': isRunning,
      'isStarted': isStarted,
      'isFinished': isFinished,
      'animationInProgress': animationInProgress,
      // Base data
      'board': board.toJson(),
      // Game data
      'currentPlayer': currentPlayer,
    };
  }
}
