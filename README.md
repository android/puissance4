# Puissance4

Four-in-a-row game in Flutter

## Game modes

-   Player vs Player (only local)
-   Player vs Cpu
    -   Dumb
    -   Hard
    -   Hardest
-   Demo (Cpu Hard vs Cpu Hardest)

## Getting Started

Run the application to play :)

`flutter run`

## Contributions

Contributions of any kind are more than welcome! Feel free to fork and improve the project in any way you want, make a pull request, or open an issue.
